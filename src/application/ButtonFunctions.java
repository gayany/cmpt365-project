package application;

import java.awt.AWTException;
import java.util.concurrent.ThreadLocalRandom;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.ToolTipManager;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

public class ButtonFunctions implements Initializable, ChangeListener {

	@FXML
	Button loadButton, saveButton, encodeButton, decodeButton;
	@FXML
	ImageView imageView;
	FileChooser fileChooser = new FileChooser();
	@FXML
	Slider brightnessSlider, contrastSlider;
	@FXML
	Button filter1, filter2, hideButton, flipButton, rotateButton, watermark, dither, redeye;

	BufferedImage bufferedImage;
	int w, h, originalW, originalH, imageValues[], imageRGB[], originalValues[], originalRGB[], currentRGB[];

//	int xshift, yshift;
	// for red eye fix
	int x1, y1, x2, y2, xstart, ystart, xend, yend;
	boolean redEyeSelected = false;
	int selectedW = 0, selectedH = 0;

	// upload image
	public void open(ActionEvent event) {
		File selectedFile = fileChooser.showOpenDialog(null);
		if (selectedFile != null) {
			try {
				bufferedImage = ImageIO.read(selectedFile);
				Image image = SwingFXUtils.toFXImage(bufferedImage, null);
				System.out.println(bufferedImage);

				w = bufferedImage.getWidth(null);
				h = bufferedImage.getHeight(null);
				originalW = w;
				originalH = h;
				imageValues = new int[w * h];
				originalValues = new int[w * h];
				imageRGB = new int[w * h * 3];
				originalRGB = new int[w * h * 3];
				currentRGB = new int[w * h * 3];

				PixelGrabber grabber = new PixelGrabber(bufferedImage.getSource(), 0, 0, w, h, imageValues, 0, w);

				try {
					if (grabber.grabPixels() != true) {
						try {
							throw new AWTException("Grabber returned false: " + grabber.status());
						} catch (Exception e) {
						}
						;
					}
				} catch (InterruptedException e) {
				}
				;

				imageView.setImage(new Image(selectedFile.toURI().toURL().toExternalForm()));
				imageView.setCache(true);
				originalValues = Arrays.copyOf(imageValues, imageValues.length);

				// center imageview

//				double xratio = imageView.getBoundsInParent().getWidth() / imageView.getImage().getWidth();
//				double yratio = imageView.getBoundsInParent().getHeight() / imageView.getImage().getHeight();
//	            double ratio = 0;
//	            
//	            if(xratio >= yratio) ratio = yratio;
//	            else ratio = xratio;
//	      
//	            double ww = imageView.getImage().getWidth() * ratio;
//	            double hh = imageView.getImage().getHeight() * ratio;
//	            
//	            imageView.setX((imageView.getFitWidth() - ww) / 2);
//	            imageView.setY((imageView.getFitHeight() - hh) / 2);
//	            
//	            xshift = (int) ((imageView.getFitWidth() - ww) / 2);
//	            yshift = (int) ((imageView.getFitHeight() - hh) / 2);

				int red, green, blue;
				for (int i = 0; i < h * w; ++i) {
					red = ((originalValues[i] & 0x00ff0000) >> 16);
					green = ((originalValues[i] & 0x0000ff00) >> 8);
					blue = ((originalValues[i] & 0x000000ff));

					imageRGB[i * 3] = red;
					imageRGB[i * 3 + 1] = green;
					imageRGB[i * 3 + 2] = blue;

					originalRGB[i * 3] = red;
					originalRGB[i * 3 + 1] = green;
					originalRGB[i * 3 + 2] = blue;

					currentRGB[i * 3] = red;
					currentRGB[i * 3 + 1] = green;
					currentRGB[i * 3 + 2] = blue;
				}

			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public void dither() {
		if (bufferedImage == null)
			return;

		int tempValues[];
		tempValues = new int[w * h];
		for (int i = 0; i < (h * w); i++) {
			tempValues[i] = imageRGB[(3 * i) + 1];
		}

		int ditherMatrix[] = { 0, 48, 12, 60, 3, 51, 15, 63, 32, 16, 44, 28, 35, 19, 47, 31, 8, 56, 4, 52, 11, 59, 7,
				55, 40, 24, 36, 20, 43, 27, 39, 23, 2, 50, 14, 62, 1, 49, 13, 61, 34, 18, 46, 30, 33, 17, 45, 29, 10,
				58, 6, 54, 9, 57, 5, 53, 42, 26, 38, 22, 41, 25, 37, 21 };
		int ditherMatrix2[];
		ditherMatrix2 = new int[16 * 16];
		for (int i = 0; i < 8; ++i) {
			for (int j = 0; j < 8; ++j) {
				ditherMatrix2[i * 16 + j] = ditherMatrix[i * 8 + j] * 4;
				ditherMatrix2[((i + 8) * 16 + j)] = ditherMatrix[i * 8 + j] * 4 + 2;
				ditherMatrix2[((i) * 16 + (j + 8))] = ditherMatrix[i * 8 + j] * 4 + 3;
				ditherMatrix2[((i + 8) * 16 + (j + 8))] = ditherMatrix[i * 8 + j] * 4 + 1;
			}
		}
		for (int i = 0; i < h; ++i) {
			for (int j = 0; j < w; ++j) {
				if (tempValues[i * (w) + j] > (ditherMatrix2[(i % 16) * (16) + (j % (16))])) {
					tempValues[i * (w) + j] = 255;
				} else {
					tempValues[i * (w) + j] = 0;
				}
			}
		}
		for (int i = 0; i < h * w; ++i) {
			imageRGB[i * 3] = tempValues[i];
			imageRGB[i * 3 + 1] = tempValues[i];
			imageRGB[i * 3 + 2] = tempValues[i];
		}

		updateCurrent();
		updateImage();
	}

	public void watermark() {

		if (bufferedImage == null)
			return;

		BufferedImage watermark;
		int watermarkValues[];
		int wh = 0;
		int ww = 0;
		int watermarkRGB[];
		int watermarkValues2[];
		fileChooser = new FileChooser();

		File selectedFile = fileChooser.showOpenDialog(null);
		if (selectedFile != null) {
			try {
				watermark = ImageIO.read(selectedFile);
				Image image = SwingFXUtils.toFXImage(watermark, null);
				System.out.println(watermark);

				ww = watermark.getWidth(null);
				wh = watermark.getHeight(null);
				watermarkValues = new int[ww * wh];

				PixelGrabber grabber3 = new PixelGrabber(watermark.getSource(), 0, 0, ww, wh, watermarkValues, 0, ww);

				try {
					if (grabber3.grabPixels() != true) {
						try {
							throw new AWTException("Grabber returned false: " + grabber3.status());
						} catch (Exception e) {
						}
						;
					}
				} catch (InterruptedException e) {
				}
				;

				watermarkValues2 = new int[ww * wh];
				watermarkValues2 = Arrays.copyOf(watermarkValues, watermarkValues.length);
				int red, green, blue;
				watermarkRGB = new int[ww * wh * 3];
				for (int i = 0; i < wh * ww; ++i) {
					red = ((watermarkValues[i] & 0x00ff0000) >> 16);
					green = ((watermarkValues[i] & 0x0000ff00) >> 8);
					blue = ((watermarkValues[i] & 0x000000ff));
					watermarkRGB[i * 3] = red;
					watermarkRGB[i * 3 + 1] = green;
					watermarkRGB[i * 3 + 2] = blue;
				}
				for (int i = 0; i < h; ++i) {
					for (int j = 0; j < w * 3; ++j) {
						imageRGB[i * (w * 3) + j] = (int) ((watermarkRGB[(i % wh) * (ww * 3) + (j % (ww * 3))] * 0.15)
								+ imageRGB[(i) * (w * 3) + j] * 0.85);
					}
				}
				updateCurrent();
				updateImage();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public void flip() {

		if (bufferedImage == null)
			return;

		int tempValues[];
		tempValues = new int[w * h];
		for (int i = 0; i < (h * w); i++) {
			tempValues[i] = (imageRGB[3 * i] << 16) + (imageRGB[(3 * i) + 1] << 8) + imageRGB[(3 * i) + 2];
		}

		for (int i = 0; i < h - 1; i++) {
			for (int j = 0; j < Math.floor(w / 2); j++) {
				int temp = tempValues[w * i + j];
				tempValues[w * i + j] = tempValues[w * (1 + i) - j];
				tempValues[w * (1 + i) - j] = temp;
			}
		}

//		for(int i = 0; i < h-1; i++) {
//		for(int j = 0; j < Math.floor(w/2); j++) {
//			
//			int temp1 = imageRGB[w*i + j];
//			int temp2 = imageRGB[w*i + j+1];
//			int temp3 = imageRGB[w*i + j+2];
//			
//			imageRGB[w*i + j] = imageRGB[w*(1+i) - j-2];
//			imageRGB[w*i + j+1] = imageRGB[w*(1+i) - j-1];
//			imageRGB[w*i + j+2] = imageRGB[w*(1+i) - j];	
//			
//			imageRGB[w*(1+i) - j-2] = temp1;
//			imageRGB[w*(1+i) - j-1] = temp2;
//			imageRGB[w*(1+i) - j] = temp3;
//		}
//	}

		int red, green, blue;
		for (int i = 0; i < h * w; ++i) {
			red = ((tempValues[i] & 0x00ff0000) >> 16);
			green = ((tempValues[i] & 0x0000ff00) >> 8);
			blue = ((tempValues[i] & 0x000000ff));

			imageRGB[i * 3] = red;
			imageRGB[i * 3 + 1] = green;
			imageRGB[i * 3 + 2] = blue;

			originalRGB[i * 3] = red;
			originalRGB[i * 3 + 1] = green;
			originalRGB[i * 3 + 2] = blue;

			currentRGB[i * 3] = red;
			currentRGB[i * 3 + 1] = green;
			currentRGB[i * 3 + 2] = blue;
		}
		updateImage();
		updateCurrent();
	}

	public void rotate() {

		if (bufferedImage == null)
			return;

		int tempValues[];
		tempValues = new int[w * h];
		for (int i = 0; i < (h * w); i++) {
			tempValues[i] = (imageRGB[3 * i] << 16) + (imageRGB[(3 * i) + 1] << 8) + imageRGB[(3 * i) + 2];
		}

		int w2 = h;
		int h2 = w;
		int tempArr[] = new int[w2 * h2];

		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				tempArr[((j + 1) * h) - i - 1] = tempValues[(i * w) + j];
			}
		}

		h = h2;
		w = w2;
		tempArr = Arrays.copyOf(tempArr, tempArr.length);

		int red, green, blue;
		for (int i = 0; i < h * w; ++i) {
			red = ((tempArr[i] & 0x00ff0000) >> 16);
			green = ((tempArr[i] & 0x0000ff00) >> 8);
			blue = ((tempArr[i] & 0x000000ff));

			imageRGB[i * 3] = red;
			imageRGB[i * 3 + 1] = green;
			imageRGB[i * 3 + 2] = blue;

			currentRGB[i * 3] = red;
			currentRGB[i * 3 + 1] = green;
			currentRGB[i * 3 + 2] = blue;
		}

		updateImage();
		updateCurrent();
	}

	public void redEyeFix() {

		if (bufferedImage == null)
			return;

		double xratio = imageView.getBoundsInParent().getWidth() / imageView.getImage().getWidth();
		double yratio = imageView.getBoundsInParent().getHeight() / imageView.getImage().getHeight();

		imageView.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				x1 = (int) mouseEvent.getX();
				y1 = (int) mouseEvent.getY();

//				System.out.println("Clicked -> X: " + x1 + ", Y: " + y1);
				imageView.removeEventFilter(MouseEvent.MOUSE_PRESSED, this);
			}
		});
		imageView.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				x2 = (int) mouseEvent.getX();
				y2 = (int) mouseEvent.getY();
				if (x2 < 0)
					x2 = 0;
				if (x2 > imageView.getFitWidth())
					x2 = (int) imageView.getFitWidth();

				if (y2 < 0)
					y2 = 0;
				if (y2 > imageView.getFitHeight())
					y2 = (int) imageView.getFitHeight();

//				System.out.println("Released -> X: " + x2 + ", Y: " + y2);
				selectedW = (int) ((x2 - x1) / xratio);
				xstart = (int) (x1 / xratio);
				xend = (int) (x2 / xratio);
				// if(x2 > x1) {selectedW = (int) ((x2-x1)/xratio); xstart = (int) (x1/xratio);
				// xend = (int) (x2/xratio);}
				if (x2 < x1) {
					selectedW = (int) ((x1 - x2) / xratio);
					xstart = (int) (x2 / xratio);
					xend = (int) (x1 / xratio);
				}
				selectedH = (int) ((y2 - y1) / yratio);
				ystart = (int) (y1 / yratio);
				yend = (int) (y2 / xratio);
				// if(y2 > y1) {selectedH = (int) ((y2-y1)/yratio); ystart = (int) (y1/yratio);
				// yend = (int) (y2/xratio);}
				if (y2 < y1) {
					selectedH = (int) ((y1 - y2) / yratio);
					ystart = (int) (y2 / yratio);
					yend = (int) (y1 / xratio);
				}
				// System.out.println("width: " + selectedW + " xstart: " + xstart + " xend: " +
				// xend);
				// System.out.println("height: " + selectedH + " ystart: " + ystart + " yend: "
				// + yend);

				// System.out.println("differentce " + Math.abs((xend - xstart) * (yend -
				// ystart)));//((xend + w * yend) - (xstart + w * ystart)));

//				System.out.println("What's the difference between me and you " + w);
				for (int i = ystart; i < yend; i++) { // height
					for (int j = xstart; j < xend; j++) {
						int k = i * 3;
						int l = j * 3;

//						System.out.println("before red: " + imageRGB[k * w + l]);
						// if(imageRGB[k*w + l] > 80 && imageRGB[k*w + l+1] + imageRGB[k*w + l+2] < 100)
						// {
//							if ((imageRGB[k * w + l] > (1 * (imageRGB[k * w + l + 1] + imageRGB[k * w + l + 2])))
//									&& (imageRGB[k * w + l] > 50) && (imageRGB[k * w + l + 1] < 50)
//									&& (imageRGB[k * w + l + 2] < 50)) {
//								imageRGB[k * w + l] = imageRGB[k * w + l] / 3;
//								imageRGB[k * w + l + 1] = imageRGB[k * w + l + 1] * 2;
//								imageRGB[k * w + l + 2] = imageRGB[k * w + l + 2] * 2;
//							}

//						if(((imageRGB[k * w + l + 1] + imageRGB[k * w + l + 2]) / 2) == 0) {
//							System.out.println("g: " + (imageRGB[k * w + l + 1]) + ", b: " + ((imageRGB[k * w + l + 2]) / 2));
//							imageRGB[k * w + l] = 0;
//						}else 

						if (1.7 < (double) (imageRGB[k * w + l]
								/ ((double) (imageRGB[k * w + l + 1] + (double) imageRGB[k * w + l + 2]) / 2))) {
//							System.out.println("2 < " + (double)(imageRGB[k * w + l] / ((double)(imageRGB[k * w + l + 1] + (double)imageRGB[k * w + l + 2]) / 2)));
							imageRGB[k * w + l] = ((imageRGB[k * w + l + 1] + imageRGB[k * w + l + 2]) / 2);
						}
					}
					updateImage();
					updateCurrent();
					imageView.removeEventFilter(MouseEvent.MOUSE_RELEASED, this);
//				System.out.println("redeyeselected: " + redEyeSelected);
				}
			}
		});
	}

	// save image
	public void save() {
		if (bufferedImage == null)
			return;

		String chooserTitle = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

		JFileChooser fileChooser2 = new JFileChooser();
		fileChooser2.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser2.showSaveDialog(null);
//
//           System.out.println(bufferedImage);
//           System.out.println(fileChooser2.getSelectedFile().getAbsolutePath());

		try {
			File f = new File(fileChooser2.getSelectedFile().getAbsolutePath() + "\\" + chooserTitle + ".png"); // output
																												// file
																												// path

			System.out.println(f);
			ImageIO.write(bufferedImage, "png", f);
			System.out.println("Writing complete.");
		} catch (IOException e) {
			System.out.println("Error: " + e);
		}
	}

	public void showFilters() {

		// Show filter buttons
		filter1.setVisible(true);
		filter2.setVisible(true);
		watermark.setVisible(true);
		dither.setVisible(true);

		// fix
		redeye.setVisible(false);
		flipButton.setVisible(false);
		rotateButton.setVisible(false);

		encodeButton.setVisible(false);
		decodeButton.setVisible(false);

	}

	public void showFix() {
		// Hide filter buttons
		filter1.setVisible(false);
		filter2.setVisible(false);
		watermark.setVisible(false);
		dither.setVisible(false);

		// fix
		redeye.setVisible(true);
		flipButton.setVisible(true);
		rotateButton.setVisible(true);

		encodeButton.setVisible(false);
		decodeButton.setVisible(false);
	}

	public void showHide() {

		filter1.setVisible(false);
		filter2.setVisible(false);
		watermark.setVisible(false);
		dither.setVisible(false);

		redeye.setVisible(false);
		flipButton.setVisible(false);
		rotateButton.setVisible(false);

		encodeButton.setVisible(true);
		decodeButton.setVisible(true);

	}

	public void reset() {

		if (bufferedImage == null)
			return;

//		imageRGB = Arrays.copyOf(originalRGB, originalRGB.length);
		contrastSlider.setValue(0);
		brightnessSlider.setValue(0);
		w = originalW;
		h = originalH;
		for (int i = 0; i < h * w; ++i) {
			imageRGB[i * 3] = originalRGB[i * 3];
			imageRGB[i * 3 + 1] = originalRGB[i * 3 + 1];
			imageRGB[i * 3 + 2] = originalRGB[i * 3 + 2];
		}
		updateCurrent();
		updateImage();

	}

	// Encode
	public void encodeImage() {
		if (bufferedImage == null)
			return;

		BufferedImage hideImage;
		int hideImageValues[];
		int hh = 0;
		int hw = 0;
		int hiddenRGB[];
		int hiddenValues[];
		fileChooser = new FileChooser();

		File selectedFile = fileChooser.showOpenDialog(null);
		if (selectedFile != null) {
			try {
				hideImage = ImageIO.read(selectedFile);
				Image image = SwingFXUtils.toFXImage(hideImage, null);
				System.out.println(hideImage);

				hw = hideImage.getWidth(null);
				hh = hideImage.getHeight(null);
				
				
				hideImageValues = new int[hw * hh];

				PixelGrabber grabber3 = new PixelGrabber(hideImage.getSource(), 0, 0, hw, hh, hideImageValues, 0, hw);

				try {
					if (grabber3.grabPixels() != true) {
						try {
							throw new AWTException("Grabber returned false: " + grabber3.status());
						} catch (Exception e) {
						}
						;
					}
				} catch (InterruptedException e) {
				}
				;

				hiddenValues = new int[hw * hh];
				hiddenValues = Arrays.copyOf(hideImageValues, hideImageValues.length);
				int red, green, blue;
				hiddenRGB = new int[hw * hh * 3];
				for (int i = 0; i < hh * hw; ++i) {
					red = ((hiddenValues[i] & 0x00ff0000) >> 16);
					green = ((hiddenValues[i] & 0x0000ff00) >> 8);
					blue = ((hiddenValues[i] & 0x000000ff));
					hiddenRGB[i * 3] = red;
					hiddenRGB[i * 3 + 1] = green;
					hiddenRGB[i * 3 + 2] = blue;
				}
				if (hw * hh * 3 <= h * w) {
					System.out.println("Colour Encode");
					if (imageRGB[0] < 128) {
						imageRGB[0] = imageRGB[0] + 8;
					} else {
						imageRGB[0] = imageRGB[0] - 8;
					}

					if (imageRGB[hw] < 128) {
						imageRGB[hw] = imageRGB[hw] + 8;
					} else {
						imageRGB[hw] = imageRGB[hw] - 8;
					}
					for (int i = 0; i < hh; i++) {
						for (int j = 0; j < hw; j++) {
							int k = j * 3;
							int l = ((i * hw) + j) * 9;
							imageRGB[l + 8] = imageRGB[l + 8];
							if (imageRGB[l] < 128) {
								imageRGB[l] = imageRGB[l] + ((hiddenRGB[(i * hw * 3) + (k)] & 0b11100000) >> 5);
							} else {
								imageRGB[l] = imageRGB[l] - ((hiddenRGB[(i * hw * 3) + (k)] & 0b11100000) >> 5);
							}
							if (imageRGB[l + 1] < 128) {
								imageRGB[l + 1] = imageRGB[l + 1] + ((hiddenRGB[(i * hw * 3) + (k)] & 0b00011100) >> 2);
							} else {
								imageRGB[l + 1] = imageRGB[l + 1] - ((hiddenRGB[(i * hw * 3) + (k)] & 0b00011100) >> 2);
							}
							if (imageRGB[l + 2] < 128) {
								imageRGB[l + 2] = imageRGB[l + 2] + ((hiddenRGB[(i * hw * 3) + (k)] & 0b00000011));
							} else {
								imageRGB[l + 2] = imageRGB[l + 2] - ((hiddenRGB[(i * hw * 3) + (k)] & 0b00000011));
							}

							if (imageRGB[l + 3] < 128) {
								imageRGB[l + 3] = imageRGB[l + 3]
										+ ((hiddenRGB[(i * hw * 3) + (k + 1)] & 0b11100000) >> 5);
							} else {
								imageRGB[l + 3] = imageRGB[l + 3]
										- ((hiddenRGB[(i * hw * 3) + (k + 1)] & 0b11100000) >> 5);
							}
							if (imageRGB[l + 4] < 128) {
								imageRGB[l + 4] = imageRGB[l + 4]
										+ ((hiddenRGB[(i * hw * 3) + (k + 1)] & 0b00011100) >> 2);
							} else {
								imageRGB[l + 4] = imageRGB[l + 4]
										- ((hiddenRGB[(i * hw * 3) + (k + 1)] & 0b00011100) >> 2);
							}
							if (imageRGB[l + 5] < 128) {
								imageRGB[l + 5] = imageRGB[l + 5] + ((hiddenRGB[(i * hw * 3) + (k + 1)] & 0b00000011));
							} else {
								imageRGB[l + 5] = imageRGB[l + 5] - ((hiddenRGB[(i * hw * 3) + (k + 1)] & 0b00000011));
							}

							if (imageRGB[l + 6] < 128) {
								imageRGB[l + 6] = imageRGB[l + 6]
										+ ((hiddenRGB[(i * hw * 3) + (k + 2)] & 0b11100000) >> 5);
							} else {
								imageRGB[l + 6] = imageRGB[l + 6]
										- ((hiddenRGB[(i * hw * 3) + (k + 2)] & 0b11100000) >> 5);
							}
							if (imageRGB[l + 7] < 128) {
								imageRGB[l + 7] = imageRGB[l + 7]
										+ ((hiddenRGB[(i * hw * 3) + (k + 2)] & 0b00011100) >> 2);
							} else {
								imageRGB[l + 7] = imageRGB[l + 7]
										- ((hiddenRGB[(i * hw * 3) + (k + 2)] & 0b00011100) >> 2);
							}
							if (imageRGB[l + 8] < 128) {
								imageRGB[l + 8] = imageRGB[l + 8] + ((hiddenRGB[(i * hw * 3) + (k + 2)] & 0b00000011));
							} else {
								imageRGB[l + 8] = imageRGB[l + 8] - ((hiddenRGB[(i * hw * 3) + (k + 2)] & 0b00000011));
							}
						}
					}
				} else {
					System.out.println("Grayscale Encode");
					for (int i = 0; i < hh; i++) {
						for (int j = 0; j < hw; j++) {
							int k = j * 3;
							int l = ((i * hw) + j) * 9;
							if (imageRGB[i * w * 3 + k] < 128) {
								imageRGB[i * w * 3 + k] = imageRGB[i * w * 3 + k]
										+ ((hiddenRGB[(i * hw * 3) + (k)] & 0b11100000) >> 5);
							} else {
								imageRGB[i * w * 3 + k] = imageRGB[i * w * 3 + k]
										- ((hiddenRGB[(i * hw * 3) + (k)] & 0b11100000) >> 5);
							}
							if (imageRGB[i * w * 3 + k + 1] < 128) {
								imageRGB[i * w * 3 + k + 1] = imageRGB[i * w * 3 + k + 1]
										+ ((hiddenRGB[(i * hw * 3) + (k)] & 0b00011100) >> 2);
							} else {
								imageRGB[i * w * 3 + k + 1] = imageRGB[i * w * 3 + k + 1]
										- ((hiddenRGB[(i * hw * 3) + (k)] & 0b00011100) >> 2);
							}
							if (imageRGB[i * w * 3 + k + 2] < 128) {
								imageRGB[i * w * 3 + k + 2] = imageRGB[i * w * 3 + k + 2]
										+ ((hiddenRGB[(i * hw * 3) + (k)] & 0b00000011));
							} else {
								imageRGB[i * w * 3 + k + 2] = imageRGB[i * w * 3 + k + 2]
										- ((hiddenRGB[(i * hw * 3) + (k)] & 0b00000011));
							}
						}
					}
				}
				updateCurrent();
				updateImage();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}

	}

	public void decodeImage() {
		if (bufferedImage == null)
			return;

		BufferedImage unhideImage;
		int unhideImageValues[];
		int uh = 0;
		int uw = 0;
		int decodeRGB[];
		int decodeValues[];
		fileChooser = new FileChooser();

		File selectedFile = fileChooser.showOpenDialog(null);
		if (selectedFile != null) {
			try {
				unhideImage = ImageIO.read(selectedFile);
				Image image = SwingFXUtils.toFXImage(unhideImage, null);
				System.out.println(unhideImage);

				uw = unhideImage.getWidth(null);
				uh = unhideImage.getHeight(null);
				unhideImageValues = new int[uw * uh];

				PixelGrabber grabber3 = new PixelGrabber(unhideImage.getSource(), 0, 0, uw, uh, unhideImageValues, 0,
						uw);

				try {
					if (grabber3.grabPixels() != true) {
						try {
							throw new AWTException("Grabber returned false: " + grabber3.status());
						} catch (Exception e) {
						}
						;
					}
				} catch (InterruptedException e) {
				}
				;

				decodeValues = new int[uw * uh];
				decodeValues = Arrays.copyOf(unhideImageValues, unhideImageValues.length);
				int red, green, blue;
				decodeRGB = new int[uw * uh * 3];
				for (int i = 0; i < uh * uw; ++i) {
					red = ((decodeValues[i] & 0x00ff0000) >> 16);
					green = ((decodeValues[i] & 0x0000ff00) >> 8);
					blue = ((decodeValues[i] & 0x000000ff));
					decodeRGB[i * 3] = red;
					decodeRGB[i * 3 + 1] = green;
					decodeRGB[i * 3 + 2] = blue;
				}
				if (Math.abs(imageRGB[0] - decodeRGB[0]) > 7) {
					int width = 0;
					int tempRGB[];
					tempRGB = new int[w * h * 3];
					for (int i = 1; i < uh * uw * 3; ++i) {
						if (Math.abs(imageRGB[i] - decodeRGB[i]) > 7) {
							width = i;
							break;
						}
					}
					for (int i = 0; i < uh * uw; ++i) {
						int j = i * 3;
						int part1 = Math.abs(imageRGB[j] - decodeRGB[j]);
						int part2 = Math.abs(imageRGB[j + 1] - decodeRGB[j + 1]);
						int part3 = Math.abs(imageRGB[j + 2] - decodeRGB[j + 2]);
						int trueValue = (part1 << 5) + (part2 << 2) + part3;
						tempRGB[(((i / (width * 3)) * uw * 3) + (i % (width * 3)))] = trueValue;
					}
					for (int i = 0; i < 3 * uh * uw; ++i) {
						imageRGB[i] = tempRGB[i];
					}
				} else {
					for (int i = 0; i < uh * uw; ++i) {
						int j = i * 3;
						int part1 = Math.abs(imageRGB[j] - decodeRGB[j]);
						int part2 = Math.abs(imageRGB[j + 1] - decodeRGB[j + 1]);
						int part3 = Math.abs(imageRGB[j + 2] - decodeRGB[j + 2]);
						int trueValue = (part1 << 5) + (part2 << 2) + part3;
						imageRGB[j] = trueValue;
						imageRGB[j + 1] = trueValue;
						imageRGB[j + 2] = trueValue;
					}
				}

//	    		else {
//					for (int i = 0; i < hh; i++) {
//						for (int j = 0; j < hw; j++) {
//							int k = j*3;
//							if(imageRGB[i*w*3 + k] <128) {
//								imageRGB[i*w*3 + k] = imageRGB[i*w*3 + k] + ((hiddenRGB[(i*hw*3) + (k+1)] & 0b11100000) >> 5);
//							}
//							else {
//								imageRGB[i*w*3 + k] = imageRGB[i*w*3 + k] - ((hiddenRGB[(i*hw*3) + (k+1)] & 0b11100000) >> 5);
//							}
//							if(imageRGB[i*w*3 + k+1] <128) {
//								imageRGB[i*w*3 + k+1] = imageRGB[i*w*3 + k+1] + ((hiddenRGB[(i*hw*3) + (k+1)] & 0b00011100) >> 2);
//							}
//							else {
//								imageRGB[i*w*3 + k+1] = imageRGB[i*w*3 + k+1] - ((hiddenRGB[(i*hw*3) + (k+1)] & 0b00011100) >> 2);
//							}
//							if(imageRGB[i*w*3 + k+1] <128) {
//								imageRGB[i*w*3 + k+2] = imageRGB[i*w*3 + k+2] + ((hiddenRGB[(i*hw*3) + (k+1)] & 0b00000011));
//							}
//							else {
//								imageRGB[i*w*3 + k+2] = imageRGB[i*w*3 + k+2] - ((hiddenRGB[(i*hw*3) + (k+1)] & 0b00000011));
//							}
//						}
//					}
//	    		}
				updateCurrent();
				updateImage();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}

	}

	// filters
	public void filter1() {
		if (bufferedImage == null)
			return;

		double psychFilter = 25;
		int threshhold = 10;
		for (int i = 0; i < w * h; i++) {
			int j = i * 3;
			int max = Math.max(Math.max(imageRGB[j], imageRGB[j + 1]), imageRGB[j + 2]);
			int min = Math.min(Math.min(imageRGB[j], imageRGB[j + 1]), imageRGB[j + 2]);
			if (Math.abs(min - max) >= threshhold * 4) {
				for (int k = 0; k < 3; k++) {
					if (imageRGB[k + j] == max) {
						imageRGB[k + j] = (int) (imageRGB[k + j] + psychFilter);
						if (imageRGB[k + j] > 255)
							imageRGB[k + j] = 255;
						if (imageRGB[k + j] < 0)
							imageRGB[k + j] = 0;
					} else {
						if (imageRGB[k + j] != min) {
							if ((imageRGB[k + j] + threshhold) >= max) {
								imageRGB[k + j] = (int) (max + psychFilter);
								if (imageRGB[k + j] > 255)
									imageRGB[k + j] = 255;
								if (imageRGB[k + j] < 0)
									imageRGB[k + j] = 0;
							}
						}
					}
				}
			} else {
				for (int k = 0; k < 3; k++) {
					if (imageRGB[k + j] == max) {
						imageRGB[k + j] = (int) (imageRGB[k + j]
								+ ((psychFilter / 5) - ((Math.abs(min - max) / (threshhold * 4)) * (psychFilter / 5))));
						if (imageRGB[k + j] > 255)
							imageRGB[k + j] = 255;
						if (imageRGB[k + j] < 0)
							imageRGB[k + j] = 0;
					} else {
						if (imageRGB[k + j] != min) {
							if ((imageRGB[k + j] + threshhold) >= max) {
								imageRGB[k + j] = (int) (max + ((psychFilter / 5)
										- ((Math.abs(min - max) / (threshhold * 4)) * (psychFilter / 5))));
								if (imageRGB[k + j] > 255)
									imageRGB[k + j] = 255;
								if (imageRGB[k + j] < 0)
									imageRGB[k + j] = 0;
							}
						}
					}
				}
			}
		}
		updateCurrent();
		updateImage();
	}

	public void filter2() {

		if (bufferedImage == null)
			return;

		double[][] swapMatrix = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		for (int i = 0; i < 3; i++) {
			double a = -1;
			double b = -1;
			double c = -1;
			while ((a < 0) && (b < 0) && (c < 0)) {
				a = ThreadLocalRandom.current().nextInt(-10, 11);
				b = ThreadLocalRandom.current().nextInt(-10, 11);
				c = ThreadLocalRandom.current().nextInt(-10, 11);
			}
			double positiveTotal = 0;
			double negativeTotal = 0;
			if (((a >= 0) && (b >= 0) && (c >= 0))) {
				a = a / (a + b + c);
				b = b / (a + b + c);
				c = c / (a + b + c);
			} else {
				if (a >= 0) {
					positiveTotal = positiveTotal + a;
				} else {
					negativeTotal = negativeTotal + a;
				}
				if (b >= 0) {
					positiveTotal = positiveTotal + b;
				} else {
					negativeTotal = negativeTotal + b;
				}
				if (c >= 0) {
					positiveTotal = positiveTotal + c;
				} else {
					negativeTotal = negativeTotal + c;
				}
				negativeTotal = Math.abs(negativeTotal);
				if (a >= 0) {
					a = 2 * (a / positiveTotal);
				} else {
					a = a / negativeTotal;
				}
				if (b >= 0) {
					b = 2 * (b / positiveTotal);
				} else {
					b = b / negativeTotal;
				}
				if (c >= 0) {
					c = 2 * (c / positiveTotal);
				} else {
					c = c / negativeTotal;
				}
			}
			swapMatrix[i][0] = a;
			swapMatrix[i][1] = b;
			swapMatrix[i][2] = c;
		}

		for (int i = 0; i < w * h; i++) {
			int j = i * 3;
			int temp1 = (int) (swapMatrix[0][0] * imageRGB[j] + swapMatrix[0][1] * imageRGB[j + 1]
					+ swapMatrix[0][2] * imageRGB[j + 2]);
			int temp2 = (int) (swapMatrix[1][0] * imageRGB[j] + swapMatrix[1][1] * imageRGB[j + 1]
					+ swapMatrix[1][2] * imageRGB[j + 2]);
			int temp3 = (int) (swapMatrix[2][0] * imageRGB[j] + swapMatrix[2][1] * imageRGB[j + 1]
					+ swapMatrix[2][2] * imageRGB[j + 2]);
			imageRGB[j] = temp1;
			imageRGB[j + 1] = temp2;
			imageRGB[j + 2] = temp3;
		}
		for (int i = 0; i < w * h * 3; i++) {
			if (imageRGB[i] > 255)
				imageRGB[i] = 255;
			if (imageRGB[i] < 0)
				imageRGB[i] = 0;
		}
		updateCurrent();
		updateImage();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		brightnessSlider.valueProperty().addListener(this);
		contrastSlider.valueProperty().addListener(this);
		filter1.setVisible(true);
		filter2.setVisible(true);
		watermark.setVisible(true);
		dither.setVisible(true);

		redeye.setVisible(false);
		flipButton.setVisible(false);
		rotateButton.setVisible(false);

		encodeButton.setVisible(false);
		decodeButton.setVisible(false);
		
		filter1.setTooltip(new Tooltip("Enhance the colours of the image."));
		filter2.setTooltip(new Tooltip("Change each colour of the image to a different randomly generated colour."));
		brightnessSlider.setTooltip(new Tooltip("Change the brightness."));
		contrastSlider.setTooltip(new Tooltip("Change the contrast."));
		watermark.setTooltip(new Tooltip("Upload a watermark to add to your image. Must be a jpeg."));
		dither.setTooltip(new Tooltip("Convert the image into a grayscale image with only black and white pixels."));
		redeye.setTooltip(new Tooltip("Click and drag to select the red eye area for it to be fixed."));
		flipButton.setTooltip(new Tooltip("Flip the image horizontally."));
		rotateButton.setTooltip(new Tooltip("Rotate the image by 90 degrees."));
		encodeButton.setTooltip(new Tooltip("Upload an image to be hidden. Must be smaller than the image on the canvas."));
		decodeButton.setTooltip(new Tooltip("Upload the original verison of the image on the canvas to decode the hidden image."));
	}

	// change brightness
	@Override
	public void changed(ObservableValue arg0, Object arg1, Object arg2) {

		if (bufferedImage == null)
			return;

		// TODO Auto-generated method stub

		// brightness slider
		double brightness = 1;
		if (Math.abs(brightnessSlider.getValue()) == brightnessSlider.getValue()) {
			brightness = 1 + brightnessSlider.getValue() / 100;
		} else {
			brightness = 1 - Math.abs(brightnessSlider.getValue()) / 200;
		}

//        for (int i = 0; i < h * w; ++i){
//        	red = ((imageValues[i] & 0x00ff0000) >> 16);
//        	green =((imageValues[i] & 0x0000ff00) >> 8);
//        	blue = ((imageValues[i] & 0x000000ff) );
//        	
//        	imageRGB[i*3] = red;
//        	imageRGB[i*3+1] = green;
//        	imageRGB[i*3+2] = blue;
//        }

		for (int i = 0; i < w * h * 3; i++) {
			imageRGB[i] = (int) (currentRGB[i] * brightness);
			if (imageRGB[i] > 255)
				imageRGB[i] = 255;
			if (imageRGB[i] < 0)
				imageRGB[i] = 0;
		}

		// contrast slider
		double contrast = 1 + (-1 * contrastSlider.getValue()) / 100;
		if (Math.abs(contrastSlider.getValue()) == contrastSlider.getValue()) {
			contrast = Math.sqrt(Math.sqrt(contrast));
			for (int j = 0; j < w * h; j++) {
				int i = j * 3;
				if ((imageRGB[i] + imageRGB[i + 1] + imageRGB[i + 2]) / 3 < (255 / 2)) {
					imageRGB[i] = (int) (imageRGB[i] * contrast);
					if (imageRGB[i] > 255)
						imageRGB[i] = 255;
					if (imageRGB[i] < 0)
						imageRGB[i] = 0;
					imageRGB[i + 1] = (int) (imageRGB[i + 1] * contrast);
					if (imageRGB[i + 1] > 255)
						imageRGB[i + 1] = 255;
					if (imageRGB[i + 1] < 0)
						imageRGB[i + 1] = 0;
					imageRGB[i + 2] = (int) (imageRGB[i + 2] * contrast);
					if (imageRGB[i + 2] > 255)
						imageRGB[i + 2] = 255;
					if (imageRGB[i + 2] < 0)
						imageRGB[i + 2] = 0;
				} else {
					imageRGB[i] = (int) (imageRGB[i] * (1 / contrast));
					if (imageRGB[i] > 255)
						imageRGB[i] = 255;
					if (imageRGB[i] < 0)
						imageRGB[i] = 0;
					imageRGB[i + 1] = (int) (imageRGB[i + 1] * (1 / contrast));
					if (imageRGB[i + 1] > 255)
						imageRGB[i + 1] = 255;
					if (imageRGB[i + 1] < 0)
						imageRGB[i + 1] = 0;
					imageRGB[i + 2] = (int) (imageRGB[i + 2] * (1 / contrast));
					if (imageRGB[i + 2] > 255)
						imageRGB[i + 2] = 255;
					if (imageRGB[i + 2] < 0)
						imageRGB[i + 2] = 0;
				}
			}
		} else {
			contrast = contrast - 1;
			contrast = Math.sqrt(Math.sqrt(contrast));
			int mean = 255 / 2;
			for (int j = 0; j < w * h; j++) {
				int i = j * 3;
				imageRGB[i] = (int) ((mean * contrast) + (imageRGB[i] * (1 - contrast)));
				if (imageRGB[i] > 255)
					imageRGB[i] = 255;
				if (imageRGB[i] < 0)
					imageRGB[i] = 0;
				imageRGB[i + 1] = (int) ((mean * contrast) + (imageRGB[i + 1] * (1 - contrast)));
				if (imageRGB[i + 1] > 255)
					imageRGB[i + 1] = 255;
				if (imageRGB[i + 1] < 0)
					imageRGB[i + 1] = 0;
				imageRGB[i + 2] = (int) ((mean * contrast) + (imageRGB[i + 2] * (1 - contrast)));
				if (imageRGB[i + 2] > 255)
					imageRGB[i + 2] = 255;
				if (imageRGB[i + 2] < 0)
					imageRGB[i + 2] = 0;
			}
		}

		// display image
		updateImage();
	}

	void updateImage() {
		bufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		WritableRaster raster = (WritableRaster) bufferedImage.getData();
		raster.setPixels(0, 0, w, h, imageRGB);
		bufferedImage.setData(raster);

		Image img = SwingFXUtils.toFXImage(bufferedImage, null);
		imageView.setImage(img);
	}

	void updateCurrent() {
		for (int i = 0; i < h * w; ++i) {
			currentRGB[i * 3] = imageRGB[i * 3];
			currentRGB[i * 3 + 1] = imageRGB[i * 3 + 1];
			currentRGB[i * 3 + 2] = imageRGB[i * 3 + 2];
		}
	}
}
